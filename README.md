### Restore records "lost" after a restart

Knowledge origin: https://github.com/influxdata/influxdb/issues/9413

<code>
# Stop the database.
$ service influxdb stop
# Perform a backup of the data, ensure you have enough space.
$ tar -C /var/lib/influxdb -czf influxdb.backup.tar.gz .
# This file is useless now.
$ rm /var/lib/influxdb/meta/meta.db
# Export the raw data to line protocol.
  #$ influx_inspect export -datadir /var/lib/influxdb/data -out <outfile>
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -out ./export
# Delete the old data.
$ rm -r /var/lib/influxdb/{meta,data,wal}
# Restart the database.
$ service influxdb start
# If you have custom retention policies (such as shard duration or other ones different from the defaults), create those now.
# Then use influx -import to import the data you exported previously.
$ influx -import -path <path>
</code>


Alternatively:

<code>
influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -out ./export -compress
</code>


Example run:

<code>
root@901f8b99f328:/# influx_inspect export -datadir /var/lib/influxdb/data -waldir /var/lib/influxdb/wal -out outfile-4-restore.influx
writing out tsm file data for _internal/monitor...complete.
writing out wal file data for _internal/monitor...complete.
writing out tsm file data for monitoring/autogen...complete.
writing out wal file data for monitoring/autogen...complete.
writing out tsm file data for mydb/autogen...complete.
writing out wal file data for test/autogen...complete.
root@901f8b99f328:/# ls -al outfile-4-restore.influx 
-rw-r--r-- 1 root root 3864873436 Sep 29 23:53 outfile-4-restore.influx
root@901f8b99f328:/# file outfile-4-restore.influx
bash: file: command not found
root@901f8b99f328:/# less outfile-4-restore.influx
bash: less: command not found
root@901f8b99f328:/# cp outfile-4-restore.influx /var/lib/influxdb/data/
</code>


### Import data

As per https://docs.aiven.io/docs/products/influxdb/howto/migrate-data-self-hosted-influxdb-aiven

<code>
influx -import -host influx-testuser-business-demo.aivencloud.com -port 12691 -username 'avnadmin' -password 'secret' -ssl -precision rfc3339 -compressed -path ./weather.influx.gz
</code>


where,

    `host`: hostname or IP address of the InfluxDB service where the data will be imported.

    `port`: the port number for the InfluxDB service.

    `username`: the username for the InfluxDB service.

    `password`: the password for the InfluxDB service.

    `ssl`: enables SSL (Secure Socket Layer) for connecting to the InfluxDB service.

    `precision`: sets the timestamp precision for the data being imported.

    `compressed`: the data being imported is in a compressed format.

    `path`: the path to the file containing the data to be imported.


